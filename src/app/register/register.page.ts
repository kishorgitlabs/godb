import { Component, OnInit } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {


email:string="";
password:string="";
registerForm: FormGroup;




  constructor(
    
    private formBuilder: FormBuilder,
    private service:ServiceService
  ) {

    this.registerForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
      });
     
   }

  ngOnInit() {
   
    
   
  }


  insertRow(value){
this.service.insertRow(value.email,value.password);

  }
getformControl(formControl) {
    return this.registerForm.get(formControl) as FormArray;
}




}
