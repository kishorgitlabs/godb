import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  databaseObj: SQLiteObject;
  readonly database_name: string = "freaky_datatable.db";
  readonly table_name: string = "myfreakytable";
  constructor(
    private platform: Platform,
    private sqlite: SQLite,
    private router: Router
  ) {

    this.platform.ready().then(() => {
      this.createDB();
      alert('Success');
    }).catch(error => {
      alert('Error');
    })
  }

  createDB() {
    this.sqlite.create({
      name: this.database_name,
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        this.databaseObj = db;
        alert('freaky_datatable Database Created!');
        this.createTable()
      })
      .catch(e => {
        alert("error " + JSON.stringify(e))
      });
  }

  createTable() {
    this.databaseObj.executeSql(`
    CREATE TABLE IF NOT EXISTS ${this.table_name}  (pid INTEGER PRIMARY KEY, email varchar(255),password varchar(255))
    `, [])
      .then(() => {
        alert('Table Created!');
      })
      .catch(e => {
        alert("error " + JSON.stringify(e))
      });
  }


  insertRow(email, password) {
    this.databaseObj.executeSql(
      `INSERT INTO ${this.table_name} (email,password) VALUES ('${email}','${password}')`, []
    ).then(() => {
      alert("ROW INSERTED");
    }).catch((e) => {
      alert(JSON.stringify(e));
    });

  }

  getRows(email, password) {
    this.databaseObj.executeSql(`
    SELECT * FROM ${this.table_name} where  email = ? and password = ?
    `
      , [email, password])
      .then((res) => {

        if (res.rows.length > 0) {
          this.router.navigate(['/home']);
        } else {
          alert('Email Or password Incorrect')
        }

        alert(JSON.stringify(res));
        // alert('suu')
      })
      .catch(e => {
        alert("error " + JSON.stringify(e))
      });
  }

}
