import { Component, OnInit } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;
  email: string;
  password: string;


  constructor(
    private formBuilder: FormBuilder,
    private sqlite: SQLite,
    private sqlservice:ServiceService
  ) {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(4)])
    });
  }

  ngOnInit() {
   
  }

  

 async onFormSubmit() {
    if (!this.loginForm.valid) {
      this.loginForm.markAllAsTouched();
      return;
    }
    console.log(this._v)
    console.log('email', this._v.email)
    console.log('password', this._v.password)
   

this.sqlservice.getRows(this._v.email,this._v.password);

  }



  





  get _v() {
    return this.loginForm.value;
  }
  getformControl(formControl) {
    return this.loginForm.get(formControl) as FormArray;
  }


}
